package br.com.Imersao.produto.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.Imersao.produto.dto.Categoria;
import br.com.Imersao.produto.dto.Produto;
import br.com.Imersao.produto.dto.Valor;
import br.com.Imersao.produto.exception.mudarValorException;
import br.com.Imersao.produto.repositories.ProdutoRepository;

@Service
public class ProdutoService {
	@Autowired
	private ProdutoRepository produtoRepository;

	// retorna select * from de produtos
	// Iterable é uma especie de lista que ser iterada com novos valores
	public Iterable<Produto> getProdutos() {
		return produtoRepository.findAll();
	}

	// insere um produto no banco de dados
	public Produto setProduto(Produto produto) {
		produtoRepository.save(produto);
		return produto;
	}

	// coleta um produto a partir do id do produto
	public Produto getProduto(int idProduto) {
		Optional<Produto> produto = produtoRepository.findById(idProduto);
		if (produto.isPresent()) {
			return produto.get();
		}
		return null;
	}

//
	public Produto mudarValor(int idProduto, Valor valor) {
			
		if (valor.getValor() <= 0) {
			throw new mudarValorException(HttpStatus.BAD_REQUEST,"Valor negativo não é aceito");
		}
		
		Produto produto = getProduto(idProduto);
		
		if(produto != null) {
			produto.setValor(valor.getValor());
			produtoRepository.save(produto);
		}
		return produto;
		
	}

	public Iterable<Produto> getProdutos(Categoria categoria){
		return produtoRepository.findAllByCategoria(categoria);
	}
	
	public void apagarProduto(int idProduto) {
		produtoRepository.deleteById(idProduto);
	}

}

package br.com.Imersao.produto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class mudarValorException extends ResponseStatusException
{
	
	private static final long serialVersionUID = 1L;

	public mudarValorException(HttpStatus status, String texto) {
		super(status);
		throw new ResponseStatusException(status, texto);
	}
	
}

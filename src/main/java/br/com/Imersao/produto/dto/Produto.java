package br.com.Imersao.produto.dto;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
//@Table(name="tb_produtos")
public class Produto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
//	@Transient --> nao cria a coluna na tabela
	private String nome;
//	@Column(name="segmento") --> Cria a coluna com o nome que desejar
	@ManyToOne // --> relacionamento de muitos para um - N - 1
//	@OneToMany --> Relacionamento de um para muitos- 1 -N
	private Categoria categoria;
	@ManyToOne(targetEntity = Segmento.class, cascade = CascadeType.ALL)
	private Segmento segmento;
	
	private String descricao;
	@ManyToMany()
	private Double valor;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

}

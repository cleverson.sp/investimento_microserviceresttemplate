package br.com.Imersao.produto.dto;

public enum Categoria {
	CONDIMENTO, LIMPEZA, ALCOOL, LATICINIOS;
}

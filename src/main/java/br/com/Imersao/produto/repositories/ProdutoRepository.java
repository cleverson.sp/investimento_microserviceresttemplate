package br.com.Imersao.produto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.produto.dto.Categoria;
import br.com.Imersao.produto.dto.Produto;

public interface ProdutoRepository extends CrudRepository <Produto, Integer> {
	public Iterable<Produto> findAllByCategoria(Categoria categoria);

}
